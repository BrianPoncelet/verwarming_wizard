import { Injectable } from '@angular/core';

@Injectable()
export class WizardService {

  stepOneResult: string;
  stepTwoResult: string;
  stepThreeResult: string;
  stepFourResult: string;
  stepFiveResult: string;
  earlyEnd: boolean;

  constructor() { }

  setStepOneResult(result) {
    this.stepOneResult = result;
  }

  setStepTwoResult(result) {
    this.stepTwoResult = result;
  }

  setStepThreeResult(result) {
    this.stepThreeResult = result;
  }

  setStepFourResult(result) {
    this.stepFourResult = result;
  }

  setStepFiveResult(result) {
    this.stepFiveResult = result;
  }

  getStepOneResult(): string {
    return this.stepOneResult;
  }

  getStepTwoResult(): string {
    return this.stepTwoResult;
  }

  getStepThreeResult(): string {
    return this.stepThreeResult;
  }

  getStepFourResult(): string {
    return this.stepFourResult;
  }

  getStepFiveResult(): string {
    return this.stepFiveResult;
  }

  setEarlyEnd(status: boolean) {
    this.earlyEnd = status;
  }

  getEarlyEnd(): boolean {
    return this.earlyEnd;
  }
}
