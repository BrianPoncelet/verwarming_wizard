import { WizardService } from './wizard.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class StepService {

  public facet_URL = 'https://18.195.113.31/rest/v2/powertools/products/search?fields=FULL&query=:relevance';

  public step_URL = 'https://18.195.113.31/rest/v2/powertools/catalogs/powertoolsProductCatalog/Online/categories/';

  constructor(private http: HttpClient, private wizardService: WizardService) { }

  getStep(step: string) {
    return this.http.get(this.step_URL.concat(step)).toPromise();
  }

  getStepTwo() {
    console.log(this.facet_URL.concat(':allCategories:' + this.wizardService.getStepOneResult()));
    return this.http.get(this.facet_URL.concat(':allCategories:' + this.wizardService.getStepOneResult())).toPromise();
  }

  getStepThree() {
    console.log(this.facet_URL.concat(':allCategories:' + this.wizardService.getStepTwoResult()));
    return this.http.get(this.facet_URL.concat(':allCategories:' + this.wizardService.getStepTwoResult())).toPromise();
  }

  getStepFour() {
    console.log(this.facet_URL.concat(':allCategories:' + this.wizardService.getStepThreeResult()));
    return this.http.get(this.facet_URL.concat(':allCategories:' + this.wizardService.getStepThreeResult())).toPromise();
  }

  getFinal() {
    console.log(this.facet_URL.concat(':allCategories:' + this.wizardService.getStepFourResult()));
    return this.http.get(this.facet_URL.concat(':allCategories:' + this.wizardService.getStepFourResult())).toPromise();
  }

  getOptionText(option: string) {
    return this.http.get(this.step_URL.concat(option)).toPromise();
  }

  testWizardEnd(query: string) {
    return this.http.get(this.facet_URL.concat(query)).toPromise();
  }
}
