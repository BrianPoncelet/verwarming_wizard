import { StepService } from './../services/step.service';
import { WizardService } from './../services/wizard.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-wizard-four',
  templateUrl: './wizard-four.component.html',
  styleUrls: ['./wizard-four.component.css']
})
export class WizardFourComponent implements OnInit {

  choices: any[] = [];
  stepFour: any;
  stepFourAlt: any;
  end: any;

  constructor(private wizardService: WizardService, private stepService: StepService) { }

  ngOnInit() {
  }

  itemClick(event, select) {
    this.wizardService.setStepFourResult(select.id);
  }

  getStepFourOptions(event) {

        this.choices = [];

        this.stepService.getStep('step4')
        .then(stepList => {this.stepFour = stepList;
        });

        this.stepService.getStepFour()
        .then(stepList => {this.stepFourAlt = stepList;
          this.stepFourAlt.facets.forEach(facet => {
            if (facet.name === 'allCategories') {
              facet.values.forEach(value => {
                if ( value.name.indexOf('step4_') !== -1 ) {
                  this.stepService.getOptionText(value.name).then(
                    optionText => {
                      this.choices.push(optionText);
                    }
                  );
                }
              });
              if (this.choices.length = 0) {
                this.end = 5;
              }
            }
          });
        }).catch( e => {
          console.log(e);
        });
      }

}
