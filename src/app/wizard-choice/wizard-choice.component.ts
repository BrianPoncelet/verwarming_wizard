import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';

@Component({
  selector: 'app-wizard-choice',
  templateUrl: './wizard-choice.component.html',
  styleUrls: ['./wizard-choice.component.css']
})
export class WizardChoiceComponent implements OnInit {

  @Input() text: string;
  @Input() active: boolean;

  constructor() { }

  ngOnInit() {
  }

}
