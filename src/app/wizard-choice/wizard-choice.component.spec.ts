import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WizardChoiceComponent } from './wizard-choice.component';

describe('WizardChoiceComponent', () => {
  let component: WizardChoiceComponent;
  let fixture: ComponentFixture<WizardChoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WizardChoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WizardChoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
