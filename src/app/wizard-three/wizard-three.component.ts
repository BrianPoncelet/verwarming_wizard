import { Component, OnInit } from '@angular/core';
import { WizardService } from '../services/wizard.service';
import { Output } from '@angular/core';
import { StepService } from '../services/step.service';
@Component({
  selector: 'app-wizard-three',
  templateUrl: './wizard-three.component.html',
  styleUrls: ['./wizard-three.component.css']
})
export class WizardThreeComponent implements OnInit {

  stepThree: any;
  stepThreeAlt: any;
  choices: any[] = [];
  end: any;

  constructor(private wizardService: WizardService, private stepService: StepService) { }

  ngOnInit() {
  }

  itemClick(event, select) {
    this.wizardService.setStepThreeResult(select.id);
  }

  getStepThreeOptions(event) {

    this.choices = [];

    this.stepService.getStep('step3')
    .then(stepList => {this.stepThree = stepList;
    });

    this.stepService.getStepThree()
    .then(stepList => {this.stepThreeAlt = stepList;
      this.stepThreeAlt.facets.forEach(facet => {
        if (facet.name === 'allCategories') {
          facet.values.forEach(value => {
            if ( value.name.indexOf('step3_') !== -1 ) {
              this.stepService.getOptionText(value.name).then(
                optionText => {
                  this.choices.push(optionText);
                }
              );
            }
          });
          if (this.choices.length = 0) {
            this.end = 5;
          }
        }
      });
    }).catch( e => {
      console.log(e);
    });
  }

}
