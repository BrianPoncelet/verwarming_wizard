import { StepService } from './services/step.service';
import { WizardService } from './services/wizard.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ArchwizardModule } from 'ng2-archwizard';
import { OrderModule } from 'ngx-order-pipe';

import { AppComponent } from './app.component';
import { WizardOneComponent } from './wizard-one/wizard-one.component';
import { WizardChoiceComponent } from './wizard-choice/wizard-choice.component';
import { WizardTwoComponent } from './wizard-two/wizard-two.component';
import { WizardThreeComponent } from './wizard-three/wizard-three.component';
import { WizardFinalComponent } from './wizard-final/wizard-final.component';
import { WizardChoiceImageComponent } from './wizard-choice-image/wizard-choice-image.component';
import { ResultItemComponent } from './result-item/result-item.component';
import { HttpClientModule } from '@angular/common/http';
import { WizardFourComponent } from './wizard-four/wizard-four.component';

@NgModule({
  declarations: [
    AppComponent,
    WizardOneComponent,
    WizardChoiceComponent,
    WizardTwoComponent,
    WizardThreeComponent,
    WizardFinalComponent,
    WizardChoiceImageComponent,
    ResultItemComponent,
    WizardFourComponent,
  ],
  imports: [
    BrowserModule,
    ArchwizardModule,
    HttpClientModule,
    OrderModule
  ],
  providers: [WizardService,
              StepService,
             ],
  bootstrap: [AppComponent]
})
export class AppModule { }
