import { WizardService } from './../services/wizard.service';
import { Component, OnInit } from '@angular/core';
import { StepService } from '../services/step.service';

@Component({
  selector: 'app-wizard-one',
  templateUrl: './wizard-one.component.html',
  styleUrls: ['./wizard-one.component.css']
})
export class WizardOneComponent implements OnInit {

  stepOne: any;
  tempResult: any;

  constructor(private wizardService: WizardService, private stepService: StepService) { }

  ngOnInit() {
  }

  itemClick(event, select) {
    this.wizardService.setStepOneResult(select.id);
  }

  getStepOneOptions(event) {
    this.stepService.getStep('step1')
    .then(stepList => {this.stepOne = stepList;
    });
  }
}
