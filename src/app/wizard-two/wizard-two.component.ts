import { Component, OnInit, ViewChild } from '@angular/core';
import { WizardService } from '../services/wizard.service';
import { StepService } from '../services/step.service';

@Component({
  selector: 'app-wizard-two',
  templateUrl: './wizard-two.component.html',
  styleUrls: ['./wizard-two.component.css']
})
export class WizardTwoComponent implements OnInit {

  stepOneResult: string;
  stepTwo: any;
  stepTwoAlt: any;
  end = 2;
  choices: any[] = [];

  constructor(private wizardService: WizardService, private stepService: StepService) { }

  @ViewChild(WizardTwoComponent)
  public wizard: WizardTwoComponent;

  ngOnInit() {
  }

  itemClick(event, select) {
    this.wizardService.setStepTwoResult(select.id);
  }

  getStepTwoOptions(event) {

    console.log(this.wizardService.getStepOneResult());

    this.choices = [];

    this.stepService.getStep('step2')
    .then(stepList => {this.stepTwo = stepList;
    });

    this.stepService.getStepTwo()
    .then(stepList => {this.stepTwoAlt = stepList;
      this.stepTwoAlt.facets.forEach(facet => {
        if (facet.name === 'allCategories') {
          facet.values.forEach(value => {
            if ( value.name.indexOf('step2_') !== -1 ) {
              this.stepService.getOptionText(value.name).then(
                optionText => {
                  this.choices.push(optionText);
                }
              );
            }
          });
          if (this.choices.length === 0) {

          }
        }
      });
    }).catch( e => {
      console.log(e);
    });
  }
}
