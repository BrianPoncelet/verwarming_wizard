import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-wizard-choice-image',
  templateUrl: './wizard-choice-image.component.html',
  styleUrls: ['./wizard-choice-image.component.css']
})
export class WizardChoiceImageComponent implements OnInit {

  @Input() text: string;
  @Input() imageUrl: string;
  @Input() active: boolean;

  constructor() { }

  ngOnInit() {
  }

}
