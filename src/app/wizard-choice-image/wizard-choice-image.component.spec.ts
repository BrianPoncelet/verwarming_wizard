import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WizardChoiceImageComponent } from './wizard-choice-image.component';

describe('WizardChoiceImageComponent', () => {
  let component: WizardChoiceImageComponent;
  let fixture: ComponentFixture<WizardChoiceImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WizardChoiceImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WizardChoiceImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
