import { Component, OnInit } from '@angular/core';
import { StepService } from '../services/step.service';

@Component({
  selector: 'app-wizard-final',
  templateUrl: './wizard-final.component.html',
  styleUrls: ['./wizard-final.component.css']
})
export class WizardFinalComponent implements OnInit {

  results: any;

  constructor(private stepService: StepService) {
  }

  ngOnInit() {
  }

  getResults() {
    this.stepService.getFinal()
    .then(results => {this.results = results;
      this.results = this.results.products;
      console.log(this.results);
    });
  }

}
