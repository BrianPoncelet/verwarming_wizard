import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WizardFinalComponent } from './wizard-final.component';

describe('WizardFinalComponent', () => {
  let component: WizardFinalComponent;
  let fixture: ComponentFixture<WizardFinalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WizardFinalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WizardFinalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
